/**
* @author davidbrowaeys[SMS]
* @description Encapsulates all contact service layer logic for a given function or module in the application
*/
public class ContactService{
	public static void upgradeContacts(Contact[] records){
		for (Contact c : records){
			//do something here
		}
	}
	public static void createStudentMemberCards(Contact[] records){
		for (Contact c : records){
			//do something here
		}
	}
}